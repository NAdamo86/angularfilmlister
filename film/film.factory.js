(function() {
'use strict';

    angular
        .module('demo')
        .factory('FilmService', FilmService);

    FilmService.inject = ['$firebaseArray', '$firebaseObject'];
    function FilmService($firebaseArray, $firebaseObject) {
        var service = {
            getFilms:getFilms,
            getFilm:getFilm
        };
        
        return service;

        ////////////////
        function getFilms() {
            var refString = "/films";
            var ref = firebase.database().ref(refString);
            var array = $firebaseArray(ref);
            return array.$loaded();
         }
        function getFilm(id) {
            var refString = "/films/" + id;
            var ref = firebase.database().ref(refString);
            var obj = $firebaseObject(ref);
            return obj.$loaded();
         }
    }
})();