(function () {
    'use strict';

    // Usage:
    // 
    // Creates:
    // 

    angular
        .module('filmList')
        .component('filmList', {
            templateUrl: 'film/list/list.template.html',
            controller: filmListController,
            bindings: {
                films: '=',
            },
        });

    filmListController.inject = ['$location','FilmService'];
    function filmListController($location, FilmService) {
        var ctrl = this;
        ctrl.create = function(){
            $location.path('/film/create');
        }

        ////////////////

        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };
    }
})();