(function () {
    'use strict';

    // Usage:
    // 
    // Creates:
    // 

    angular
        .module('filmDetails')
        .component('filmDetails', {
            templateUrl: 'film/details/details.template.html',
            controller: filmDetailsController,
            // bindings: {
            //     Binding: '=',
            // },
        });

    filmDetailsController.inject = ['$routeParams', 'FilmService'];
    function filmDetailsController($routeParams, FilmService) {
        var ctrl = this;
        FilmService.getFilm($routeParams.id).then(function(data){
            console.log(data);
            ctrl.film=data;
        }).catch(function(e){
            console.error(e);
        });


        ////////////////

        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };
    }
})();