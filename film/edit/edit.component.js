(function () {
    'use strict';

    // Usage:
    // 
    // Creates:
    // 

    angular
        .module('filmEdit')
        .component('filmEdit', {
            templateUrl: 'film/edit/edit.template.html',
            controller: filmEditController,
            // bindings: {
            //     Binding: '=',
            // },
        });

    filmEditController.inject = ['$routeParams', '$location', 'FilmService'];
    function filmEditController($routeParams, $location, FilmService) {
        var ctrl = this;
        ctrl.submit = function () {

            if (!ctrl.film.$id) {
                //new
                FilmService.getFilm(guid()).then(function (data) {
                    console.log(data);
                    angular.forEach(ctrl.film, function(value, key){
                        data[key] = value;
                    });
                    data.$save().then(function(){
                        $location.path("/film/" + data.$id);
                    }).catch(function(e){
                        console.error("Creation failed", e);
                    });
                    
                }).catch(function (e) {
                    console.error(e);
                });
            }
            else {
                //edit
                ctrl.film.$save();
            }
        }
        ctrl.delete = function () {
            ctrl.film.$remove().then(function (data) {
                $location.path("/films");
            }).catch(function (e) {
                console.error("Remove failed", e);
            });
        }
        if ($routeParams.id) {
            FilmService.getFilm($routeParams.id).then(function (data) {
                console.log(data);
                ctrl.film = data;
            }).catch(function (e) {
                console.error(e);
            });
        }



        ////////////////

        ctrl.onInit = function () { };
        ctrl.onChanges = function (changesObj) { };
        ctrl.onDestory = function () { };

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }
    }
})();