var app = angular.module('demo', [
    'ngRoute',
    'firebase',
    'ngMaterial',
    'filmList',
    'filmDetails',
    'filmEdit'
]);