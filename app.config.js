angular.module("demo")
    .config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');
            $routeProvider
                .when('/films', {
                    template: '<film-list films="$resolve.films"></film-list>',
                    resolve:{
                        films: getFilms
                    }
                })
                .when('/film/create', {
                    template: "<film-edit></film-edit>"
                })
                .when('/film/:id', {
                    template: "<film-details></film-details>"
                })
                .when('/film/edit/:id', {
                    template: "<film-edit></film-edit>"
                })
                .otherwise('/films');

            function getFilms(FilmService) {
                return FilmService.getFilms().then(function (data) {
                    console.log(data);
                    return  data;
                }).catch(function (e) {
                    console.error(e);
                    return e;
                })
            }
        }])